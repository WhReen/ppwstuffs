from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
response = {}

# Create your views here.
def infos(request):
    return render(request, 'infos.html')