$(function(){
    $(".accord").on("click",function(){
        $(this).toggleClass("active");
        var panel = $(this).parent().next();
        if (panel.css("display") == "none")
            panel.css("display", "block")
        else
            panel.css("display", "none")
    })
    $(".swapdown").on("click",function(){
        $(this).toggleClass("active");
        var group = $(this).parent().parent();
        group.insertAfter(group.next());
    })
    $(".swapup").on("click",function(){
        $(this).toggleClass("active");
        var group = $(this).parent().parent();
        group.insertBefore(group.prev());
    })
});