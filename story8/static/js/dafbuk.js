$(function(){
    var searchbook = function(){
            $(this).toggleClass("active");
            $(".hasilsearch").empty()
            if ($(this).val().length == 0) {
                $("#dafbuk").css("display","none")
            } else {
                $("#dafbuk").css("display","block")
            }
            var rslt = $(this).val();
            $.ajax({
                url: "https://www.googleapis.com/books/v1/volumes?q="+rslt, 
                success: function(result){
                    var book = result.items;
                    for (i = 0; i < book.length; i++) {
                        $(".hasilsearch").append(
                            "<div></div><div class='bukubuku mb-3'> <h1>"+
                            book[i].volumeInfo.title+
                            "</h1>"+

                            "<img src='"+
                            book[i].volumeInfo.imageLinks.smallThumbnail+
                            "'>"+

                            "<a href='"+
                            book[i].volumeInfo.infoLink+
                            "'> Link to the book </a>"+

                            "</div>"
                        );
                    }
                }

            });
        };
    $(".searchbook").on(
        "keyup ready",searchbook);

});