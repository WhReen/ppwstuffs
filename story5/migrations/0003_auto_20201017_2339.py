# Generated by Django 3.1.1 on 2020-10-17 16:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0002_auto_20201017_2224'),
    ]

    operations = [
        migrations.AddField(
            model_name='matkul',
            name='desc',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='matkul',
            name='kelas',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='matkul',
            name='semester',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='matkul',
            name='sks',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='matkul',
            name='teacher',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
