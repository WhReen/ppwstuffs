from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from . import models, forms

response = {}
# Create your views here.
def matkul(request):
    matkuls = models.MatKul.objects.all()
    form = forms.MatkulForm()
    response['matkuls'] = matkuls

    return render(request, 'matkul.html', response)

def savematkul(request):
    if request.method == 'POST':
        matkuls = models.MatKul()
        if request.POST.get('namamatkul'):
            matkuls.name = request.POST.get('namamatkul')
            matkuls.teacher = request.POST.get('dosenmatkul')
            matkuls.sks = request.POST.get('sksmatkul')
            matkuls.desc = request.POST.get('descmatkul')
            matkuls.semester = request.POST.get('smstrmatkul')
            matkuls.kelas = request.POST.get('kelasmatkul')
            matkuls.save()
    return HttpResponseRedirect('/story5#bio')
    
def deletematkul(request):
    if request.method == 'POST':
        matkuls = models.MatKul.objects
        if request.POST.get('namamatkul'):
            m = matkuls.get(id = request.POST.get('namamatkul'))
            m.delete()
    return HttpResponseRedirect('/story5#bio')

def detail(request):
    if request.method == 'POST':
        matkuls = models.MatKul.objects
        if request.POST.get('namamatkul'):
            response['matkul'] = get_object_or_404(matkuls, id = id)
    elif request.GET['matkulid']:
        matkuls = models.MatKul.objects
        idmatkul = request.GET['matkulid']
        m = get_object_or_404(matkuls, id = idmatkul)
        response['nama'] = m.name
        response['dosen'] = m.teacher
        response['sks'] = m.sks
        response['desc'] = m.desc
        response['semester'] = m.semester
        response['kelas'] = m.kelas

    return render(request, 'detail.html', response)