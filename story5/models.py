from django.db import models

# Create your models here.
class MatKul(models.Model):
    name = models.CharField(max_length=100)
    teacher = models.CharField(max_length=100, null=True)
    sks = models.CharField(max_length=100, null=True)
    desc = models.CharField(max_length=100, null=True)
    semester = models.CharField(max_length=100, null=True)
    kelas = models.CharField(max_length=100, null=True)

    def __str__(self): 
        return self.name 