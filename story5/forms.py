from django import forms

class MatkulForm(forms.Form):
    name = forms.CharField(label='Nama Matkul', max_length=100)