from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout, models
response = {}


# Create your views here.
def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/story3/')
            ...
        else:
            # Return an 'invalid login' error message.
            response['fail'] = True
            return render(request, 'login.html',response)
    else:
        return render(request, 'login.html')

def register_view(request):
    if request.method == 'POST':
        if (request.POST.get('password') == request.POST.get('repassword')):
            username = request.POST['username']
            if models.User.objects.filter(username=username).exists():
                response['exist'] = True
            else:
                user = models.User.objects.create_user(username, request.POST.get('email'), request.POST.get('password'))
                user.save()
                return HttpResponseRedirect('/story9/login')
        else:
            response['wrong'] = True
    return render(request, 'register.html', response)

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/story3/')