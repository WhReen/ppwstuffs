from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from django.apps import apps
from .apps import Story9Config
from django.contrib.auth.models import User

# Create your tests here.
class Story9UnitTest(TestCase):

    def test_story9_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')

    def test_story9_login_url_exist(self):
        response = Client().get('/story9/login')
        self.assertEqual(response.status_code, 200)

    def test_story9_login_using_index_func(self):
        found = resolve('/story9/login')
        self.assertEqual(found.func, login_view)

    def test_story9_login_using_template(self):
        response = Client().get('/story9/login')
        self.assertTemplateUsed(response, 'login.html')

    def test_story9_register_url_exist(self):
        response = Client().get('/story9/register')
        self.assertEqual(response.status_code, 200)

    def test_story9_register_using_index_func(self):
        found = resolve('/story9/register')
        self.assertEqual(found.func, register_view)

    def test_story9_register_using_template(self):
        response = Client().get('/story9/register')
        self.assertTemplateUsed(response, 'register.html')

    def test_story9_can_POST_register_request(self):
        response = self.client.post('/story9/register',
         {'username': 'ase','password': 'password123!', 'repassword': 'password123!', 'email': 'abeced'}, )
        User.objects.filter(username="ase").exists() == True
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story9/login')

    def test_story9_can_POST_register_request_error(self):
        response = self.client.post('/story9/register',
         {'username': 'asen','password': 'password123!', 'repassword': 'password133!', 'email': 'abeced'}, )
        User.objects.filter(username="ase").exists() == False
        html_response = response.content.decode('utf8')
        self.assertIn("Password didn't match!", html_response)
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/story9/register',
         {'username': 'asean','password': 'password123!', 'repassword': 'password123!', 'email': 'abeced'}, )
        User.objects.filter(username="ase").exists() == True
        response = self.client.post('/story9/register',
         {'username': 'asean','password': 'password133!', 'repassword': 'password133!', 'email': 'abeceeed'}, )
        html_response = response.content.decode('utf8')
        self.assertIn('Username already exist!', html_response)
        self.assertEqual(response.status_code, 200)
    
    def test_story9_can_POST_login_request(self):
        response = self.client.post('/story9/register',
         {'username': 'Aseek','password': 'password123!', 'repassword': 'password123!', 'email': 'abeced'}, )
        User.objects.filter(username="Aseek").exists() == True
        response = self.client.post('/story9/login',
         {'username': 'Aseek', 'password': 'password123!'})
        authenticate(response, username='Aseek', password='password123!') != None

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story3/')

        new_response = self.client.get('/story3/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Aseek', html_response)

    def test_story9_can_POST_login_request_error(self):
        response = self.client.post('/story9/register',
         {'username': 'Aseek','password': 'password123!', 'repassword': 'password123!', 'email': 'abeced'}, )
        User.objects.filter(username="Aseek").exists() == True
        response = self.client.post('/story9/login',
         {'username': 'Aseek', 'password': 'password133!'})
        authenticate(response, username='Aseek', password='password123!') == None
        html_response = response.content.decode('utf8')
        self.assertIn('Wrong username/password!', html_response)
        self.assertEqual(response.status_code, 200)

    def test_story9_can_logout_request(self):
        response = self.client.post('/story9/register',
         {'username': 'Aseeak','password': 'password123!', 'repassword': 'password123!', 'email': 'abeced'}, )
        User.objects.filter(username="Aseek").exists() == True
        response = self.client.post('/story9/login',
         {'username': 'Aseeak', 'password': 'password123!'})
        authenticate(response, username='Aseek', password='password123!') != None
        new_response = self.client.get('/story3/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('(Logout?)', html_response)
        new_response = self.client.get("/story9/logout")
        new_response = self.client.get("/story3/")
        html_response = new_response.content.decode('utf8')
        self.assertIn('Login', html_response)



