from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *

# Create your tests here.
class Story6UnitTest(TestCase):

    def test_story6_url_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, daftarkeg)

    def test_story6_using_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'daftarkeg.html')

    def test_story6_Kegiatan_new_instance(self):
        new = Kegiatan.objects.create(keg_name='Tes')
        hitjmlh = Kegiatan.objects.all().count()
        self.assertEqual(hitjmlh,1)
        self.assertEqual(new.keg_name,'Tes')

    def test_story6_Peserta_new_instance(self):
        new = Kegiatan.objects.create(keg_name='Tes Peserta')
        pes = Peserta.objects.create(name='Anon', event=new)
        hitjmlh = Peserta.objects.all().count()
        hitjmlhkeg = Kegiatan.objects.all().count()
        self.assertEqual(hitjmlh,1)
        self.assertEqual(hitjmlhkeg,1)
        self.assertEqual(pes.event,new)
        self.assertEqual(pes.name,'Anon')

    def test_story6_can_save_POST_Kegiatan_request(self):
        response = self.client.post('/story6/addevent',
         {'keg_name': 'Tes Post'})
        hitjmlhkeg = Kegiatan.objects.all().count()
        self.assertEqual(hitjmlhkeg,1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/')

        new_response = self.client.get('/story6/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Tes Post', html_response)
    
    def test_story6_can_save_POST_Peserta_request(self):
        keg = Kegiatan.objects.create(keg_name='Tes Event')
        response = self.client.post('/story6/addpeserta',
         {'pes_name': 'Anon', 'keg_name': 'Tes Event'})
        hitjmlhkeg = Peserta.objects.all().count()
        self.assertEqual(hitjmlhkeg,1)
        hitjmlhkeg = Kegiatan.objects.all().count()
        self.assertEqual(hitjmlhkeg,1)
        pes = Peserta.objects.get(name = 'Anon')
        self.assertEqual(pes.event,keg)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/')

        new_response = self.client.get('/story6/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Anon', html_response)