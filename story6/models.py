from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    keg_name = models.CharField(max_length=100)

    def __str__(self): 
        return self.keg_name 

class Peserta(models.Model):
    name = models.CharField(max_length=100)
    event = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self): 
        return self.name

