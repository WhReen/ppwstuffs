from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .models import *
response = {}
# Create your views here.
def daftarkeg(request):
    response['kegiatan'] = Kegiatan.objects.all()
    response['peserta'] = Peserta.objects.all()
    return render(request, 'daftarkeg.html', response)

def addevent(request):
    if request.method == 'POST':
        keg = Kegiatan()
        if request.POST.get('keg_name'):
            keg.keg_name = request.POST.get('keg_name')
            keg.save()
        return HttpResponseRedirect('/story6/')

def addpeserta(request):
    if request.method == 'POST':
        pes = Peserta()
        if request.POST.get('pes_name'):
            if request.POST.get('keg_name'):
                pes.name = request.POST.get('pes_name')
                pes.event = Kegiatan.objects.get(keg_name = request.POST.get('keg_name'))
                pes.save()
        return HttpResponseRedirect('/story6/')