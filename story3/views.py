from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')
def portal(request):
    return render(request, 'portal.html')
def another(request):
    return render(request, 'another.html')